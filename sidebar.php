<div class="col-sm-3 col-sm-offset-1 blog-sidebar">

    <?php if( ! is_single() ) : // s'il n'y a qu'un seul post ?>

        <h4>À propos</h4>
        <p>   <?php the_author_meta('descripton'); ?> </p>

        <h4>Archives</h4>
        <ol class="list_unstyled">
            <?php wp_get_archives('type=monthly'); ?> <!-- accepte aussi le paramètre 'type=alpha' -->
        </ol>

    <?php else :
        $current = get_the_ID();
        ?>

        <h4>Autres articles</h4>
        <ol class="list_unstyled">
            <?php
            // pour afficher tous les posts d'un même auteur
            $auteur_posts = new WP_Query( array ( 'author' => get_the_author()->id ) );
            // version Sébastien : $auteur_posts = new WP_Query( array ( 'author' => get_the_author_meta('user_id') ) );
            while ( $auteur_posts->have_posts() ) : $auteur_posts->the_post();
                if(get_the_ID() !== $current) :
                    ?>
                    <li>
                        <a href="<?php the_permalink(); ?>">
                            <?php the_title(); ?>
                        </a>
                    </li>
                <?php endif; endwhile; ?>
        </ol>

    <?php endif; ?>

    <?php
    if ( is_active_sidebar('sidebar') ) :
        dynamic_sidebar('sidebar');
    endif;
    ?>
</div>