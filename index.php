<?php

get_header();

?>



    <div class="row">
<!--        <div class="col-sm-8 blog-main">-->
<!--            <span class="btn btn-sm btn-success" id="bonjour">Bonjour</span>-->

            <?php
            // s'il y a des posts : tant qu'il y a des posts :
//            if( have_posts() ) : while ( have_posts() ) : the_post();
//                get_template_part( 'content', get_post_format() );
//            endwhile; endif;
//            ?>


<!--        </div>-->

        <div id="carousel" class="carousel" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
                <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="http://localhost/sites_web/wp1/wp-content/uploads/2020/08/thermique-1.png" class="d-block w-100" alt="Voiture">
                    <div class="carousel-caption d-none d-md-block">
                        <h5 class="carousel-text">Voiture</h5>
                        <p class="carousel-text"></p>
                    </div>
                </div>
                <div class="carousel-item">
                    <img src="http://localhost/sites_web/wp1/wp-content/uploads/2020/08/drone-1.jpg" class="d-block w-100" alt="Drone">
                    <div class="carousel-caption d-none d-md-block">
                        <h5 class="carousel-text">Drone</h5>
                        <p class="carousel-text">.</p>
                    </div>
                </div>
                <div class="carousel-item">
                    <img src="http://localhost/sites_web/wp1/wp-content/uploads/2020/08/bateau.jpg" class="d-block w-100" alt="Bateau">
                    <div class="carousel-caption d-none d-md-block">
                        <h5 class="carousel-text">Bateau</h5>
                        <p class="carousel-text"></p>
                    </div>
                </div>
            </div>
            <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carousel" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>


        <?php
            get_sidebar();
        ?>

    </div>

<div class="row background-article">

        <?php
        //  s'il y a des posts : tant qu'il y a des posts :
        if( have_posts() ) : while ( have_posts() ) : the_post();
            get_template_part( 'content', get_post_format() );
        endwhile; endif;
        ?>

</div>


<?php

get_footer();

?>