<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>TD Wordpress</title>
    <?php
    wp_head();
    ?>

    <link rel="stylesheet" href="style.css">
    <link href="https://fonts.googleapis.com/css2?family=PT+Sans:ital,wght@0,400;0,700;1,400&family=Pacifico&display=swap" rel="stylesheet">
    <!--   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">-->

</head>
<body>
<div class="body-padding">

<?php
if ( get_header_image() ) :
    ?>
    <div id="site-header" class="header-title">
        <a href="<?php echo esc_url( home_url('/') ); ?>" rel="home">
            <img src="<?php header_image(); ?>"
                 width="<?php echo absint(get_custom_header()->width); ?>%"
                 height="<?php echo absint(get_custom_header()->height); ?>"
                 alt="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>"
            />
        </a>
    </div>

<?php endif; ?>

<div class="nav-menu">
    <?php
    wp_nav_menu(array(
            'theme_location' => 'menuTD'
    ));
    ?>
</div>

    <div id="carrousel">

    </div>

<div class="jumbotron">

    <h1 class="blog-title">

        <a href="<?php echo get_bloginfo('wpurl'); ?>"> <?php echo get_bloginfo('name'); ?>   </a>

    </h1>

    <p class="lead blog-description">   <?php echo get_bloginfo('description'); ?>   </p>

</div>

<?php
//wp_nav_menu(array(
//    'theme_location' => 'notreMenu',
//    'container' => '',
//    'menu_class' => 'navbar-nav mr-auto',
//    'menu_id' => '',
//    // le walker est un paramètre qui va parcourir chq élément du menu avec
//    // une classe qu'on va créer
//    'walker' => new ern_walker()
//));
//?>

<!--
   <ul class="navbar-nav mr-auto">
      <li class="nav-item">
         <a href="#" class="nav-link">Services</a>
      </li>
      <li class="nav-item">
         <a href="#" class="nav-link">À propos</a>
      </li>
      <li class="nav-item">
         <a href="#" class="nav-link">Contact</a>
      </li>
   </ul>
-->