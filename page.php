<?php

get_header();

?>



<div class="row">
    <div class="col-sm-8 blog-main">

        <?php
        // s'il y a des posts : tant qu'il y a des posts :
        if( have_posts() ) : while ( have_posts() ) : the_post();
            get_template_part( 'content', get_post_format() );
        endwhile; endif;
        ?>

    </div>

    <?php
    get_sidebar();
    ?>


</div>


<?php

get_footer();

?>
