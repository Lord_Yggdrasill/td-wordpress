<?php

get_header();

?>


<header class="page-header">
    <?php
    the_archive_title('<h1 class="entry-title">', '</h1>');  // fonction de titre de category
    the_archive_description('div class="taxonomy-description">', '</div>'); // fonction de description de category
    ?>
</header>

<div class="row">
    <div class="col-sm-8 blog-main">

        <?php
        // s'il y a des posts : tant qu'il y a des posts :
        if( have_posts() ) : while ( have_posts() ) : the_post();
            get_template_part( 'content', 'category' );
        endwhile; endif;
        ?>

    </div>

    <?php
    get_sidebar();
    ?>

</div>


<?php

get_footer();

?>
