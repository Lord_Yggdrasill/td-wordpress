<?php

function ern_setup()
{
    global $content_width;
    if ( !isset($content_width) ) {
        $content_width = 1250;
    }

    /**
     * ajout des liens rss
     */
    add_theme_support('automatic_feed_links');

    /**
     * ajout d'images
     */
    add_theme_support('post-thumbnail');

    $args = array (   // paramètres de l'image
        'default-image' => get_template_directory_uri().'/img/default-image.jpg',
        'default-text-color' => '000',
        'width' => '100%',
        'height' => 250,
        'flex-width' => true,
        'flex-height' => true
    );

    add_theme_support('custom-header', $args);
}

add_action('after_setup_theme', 'ern_setup');

/**
 * gestion des fichiers externes css js
 */


function ern_theme_script() {
    //style principal
    wp_register_style('main_style', get_template_directory_uri().'/style.css', array(), true);
    wp_enqueue_style('main_style');

    //feuille de style météo
    wp_register_style('meteo_style', get_template_directory_uri().'/meteo/meteo.css', array(), true);
    wp_enqueue_style('meteo_style');
    //style bootstrap
    wp_register_style('boot', 'https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css', array(), true);
    wp_enqueue_style('boot');

    wp_register_script('jquery', 'https://code.jquery.com/jquery-3.5.1.min.js', array(), true, true );
    wp_enqueue_script('jquery');

    wp_register_script('bootjs', 'https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js', array(), true, true );
    wp_enqueue_script('bootjs');

    wp_register_script('monjs', get_template_directory_uri().'/js/monJS.js', array(), true, true );
    wp_enqueue_script('monjs');
}

add_action('wp_enqueue_scripts', 'ern_theme_script');

/**
 * déclaration des espaces widget
 */

/**
 * définit les différentes sidebars qu'on va utiliser
 */
function ern_widget_init()
{
    register_sidebar(
        array (
            'name' => 'sidebar',
            'description' => __('les widgets de la sidebar', 'ern2020'),
            'id' => 'sidebar',
            'before_widget' => '<div class="border border-info mb-3 rounded p-2">',
            'after_widget' => '</div>',
            'before_title' => '<h4 class="widget-title">',
            'after_title' => '</h4>'
        )
    );

    register_sidebar(
        array (
            'name' => 'sidebar 2',
            'description' => __('les widgets de la deuxième sidebar', 'ern2020'),
            'id' => 'sidebar2',
            'before_widget' => '<div class="border border-info mb-3 rounded p-2">',
            'after_widget' => '</div>',
            'before_title' => '<h4 class="widget-title">',
            'after_title' => '</h4>'
        )
    );

    register_widget('WP_Ern_Meteo');
    register_widget('WP_Widget_Championnats');
    register_widget('WP_Widget_Stats');
}

add_action( 'widgets_init', 'ern_widget_init' );



/**
 * fonction de déclaration des menus
 */
function register_menu()
{
    register_nav_menus(
        array(
            // 'le nom qu'on lui a donné'
            'notreMenu' => __('Menu haut'),
            'menuTD' => __('Menu nav')
        )
    );
}

// ajout des menus au template lors de l'initialisation
add_action( 'init', 'register_menu' );

class ern_walker extends Walker_Nav_Menu
{
    /**
     * construit le début des éléments de notre menu
     * les variables sont construites en amont pour récupérer les éléments
     */
    function start_el(&$output, $item, $depth = 0, $args = null, $id = 0)
    {
        $title = $item->title;
        $description = $item->description;
        $permalink = $item->url;

        $output .= "<li class='nav-item'>";

        // protection si lien vide
        if( $permalink && $permalink != "#" ) {
            $output .= "<a href='". $permalink ."' class='nav-link'>";
        } else {
            $output .= "<span>";
        }

        $output .= $title;

        if($description != '' && $depth == 0) {
            $output .= "<small class='description'>". $description . "</small>";
        }

        // protection si lien vide
        if( $permalink && $permalink != "#" ) {
            $output .= "</a>";
        } else {
            $output .= "</span>";
        }
    }

}

/**
 * widget meteo
 */

class WP_Ern_Meteo extends WP_Widget
{
    public function __construct()
    {
        $widget_ops = array(
            'className' => 'ern_meteo',
            'description' => __("Informations de Meteo pour le WP de l'ern"),
            'customize_selected_refresh' => true
        );
        parent::__construct('wheather', __('Meteo Widget', 'Meteo'), $widget_ops);
    }

    // contruction du formulaire de backend
    public function form($instance)
    {
        $instance = wp_parse_args( (array) $instance,
            array(
                'lat' => '',
                'lon' => '',
                'api' => ''
            )
        );
        ?>
        <p>
            <label for="<?php echo $this->get_field_id('lat') ?>">Latitude</label>
            <input type="text"
                   class="widefat"
                   id="<?php echo $this->get_field_id('lat') ?>"
                   name="<?php echo $this->get_field_name('lat') ?>"
                   value="<?php echo esc_attr( $instance['lat'] ) ?>">
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('lon') ?>">Longitude</label>
            <input type="text"
                   class="widefat"
                   id="<?php echo $this->get_field_id('lon') ?>"
                   name="<?php echo $this->get_field_name('lon') ?>"
                   value="<?php echo esc_attr( $instance['lon'] ) ?>">
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('apitoken') ?>">Clé Api</label>
            <input type="text"
                   class="widefat"
                   id="<?php echo $this->get_field_id('apitoken') ?>"
                   name="<?php echo $this->get_field_name('apitoken') ?>"
                   value="<?php echo esc_attr( $instance['apitoken'] ) ?>">
        </p>
        <?php
    }

    //mise à jour des données
    public function update($new_instance, $old_instance)
    {
        $instance = $old_instance;
        $instance['lat'] = sanitize_text_field( $new_instance['lat'] );
        $instance['lon'] = sanitize_text_field( $new_instance['lon'] );
        $instance['apitoken'] = sanitize_text_field( $new_instance['apitoken'] );

        return $instance;
    }

    /**
     * affichage frontend
     * @param array $args
     * @param array $instance
     */
    public function widget($args, $instance)
    {
        $title = "Météo";
        $url = 'https://api.darksky.net/forecast' .
            $instance['apitoken'] . '/' . $instance['lat'].','.$instance['lon'];
        $request = wp_remote_get($url);
        if ( is_wp_error( $request )) {
            return false;
        }

        $body = wp_remote_retrieve_body( $request );
        $data = json_decode( $body );

        echo $args['before_widget'];
        if ($title) {
            echo $args['before_title'] . $title . $args['after_title'];
        }
        echo '<div class="meteo_wrap" id="meteo_wrap">';
        if (!empty($data)) {
            echo '<div class="'.$data->currently->icon.'icon">&nbsp;</div>';
            echo '<div>'.number_format((($data->currently->temperature -32) * (5/9)),
                    2, ',', ' ').' deg</div>';
            echo '<div>'.number_format(($data->currently->windSpeed  * 1.609),
                    2, ',', ' ').' km</div>';
        }
        echo '</div>';
        echo $args['after_widget'];

        return '';
    }

}

//Faire un widget qui affiche des phrases aléatoires au chargement de la page
//class WP_Citation extends WP_Widget
//{
//
//}
//
///**
// * shortcode
// */
//
//function myShortCode($atts)
//{
//    $a = shortcode_atts(['percent' => '10'], $atts);
//    return "<strong>Article en promotion {$a['percent']}%</strong>";
//}
//
//add_shortcode('promo', 'myShortCode');


// ---------Widget pour afficher le classement des championnats ------------ //
class WP_Widget_Championnats extends WP_Widget
{
    public function __construct()
    {
        $widget_ops = array(
            'className' => 'widget_championnats',
            'description' => __("Permet d'afficher le classement des différents championnats"),
            'customize_selected_refresh' => true
        );
        parent::__construct('classsement', __('Classement Widget', 'Classement'), $widget_ops);
    }

    /**
     * affichage frontend
     * @param array $args
     * @param array $instance
     */
    public function widget($args, $instance)
    {
        global $wpdb;
        $results = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}competition_drone");
        if (!empty ($results)) {
                ?>
                <div class="table-ranking">
                    <table class="table-dark table-bordered">
                        <h2>Classement Drone</h2>
                        <tr>
                            <th>position </th>
                            <th>points </th>
                            <th>nom </th>
                            <th>prenom </th>
                            <th>club </th>
                            <th>rotors </th>
                            <th>manches </th>
                            <th>courses </th>
                        </tr>
                        <?php
                            foreach ($results as $row) {
                        ?>
                            <tr>
                                <td><?php echo $row->position ?></td>
                                <td><?php echo $row->points ?></td>
                                <td><?php echo $row->nom ?></td>
                                <td><?php echo $row->prenom ?></td>
                                <td><?php echo $row->club ?></td>
                                <td><?php echo  $row->rotors ?></td>
                                <td><?php echo $row->manches ?></td>
                                <td><?php echo $row->nb_courses ?></td>
                            </tr>
                            <?php
                                }
                            ?>
                    </table>
                </div>
            <div class="table-separation"></div>
                <?php
        }
        $results = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}competition_auto");
        if (!empty ($results)) {
            ?>
                <div class="table-ranking">
                    <h2>Classement Automobile</h2>
                    <table class="table-dark table-bordered">
                        <tr>
                            <th>position </th>
                            <th>points </th>
                            <th>nom </th>
                            <th>prenom </th>
                            <th>club </th>
                            <th>courses </th>
                        </tr>
                        <?php
                        foreach ($results as $row) {
                            ?>
                            <tr>
                                <td><?php echo $row->position ?></td>
                                <td><?php echo $row->points ?></td>
                                <td><?php echo $row->nom ?></td>
                                <td><?php echo $row->prenom ?></td>
                                <td><?php echo $row->club ?></td>
                                <td><?php echo $row->nb_courses ?></td>
                            </tr>
                            <?php
                        }
                        ?>
                    </table>
                </div>
            <?php
        }
    }
}

class WP_Widget_Stats extends WP_Widget
{
    public function __construct()
    {
        $widget_ops = array(
            'className' => 'widget_statistiques',
            'description' => __("Informations sur la répartition des régions et des catégories les plus pratiquées"),
            'customize_selected_refresh' => true
        );
        parent::__construct('stats', __('Stats Widget', 'Stats'), $widget_ops);
    }


    /**
     * affichage frontend
     * @param array $args
     * @param array $instance
     */
    public function widget($args, $instance)
    {
        global $wpdb;
        $results = $wpdb->get_results("SELECT COUNT(id) AS nb_clubs, region FROM {$wpdb->prefix}clubs GROUP BY region");
        $result2 = $wpdb->get_results("SELECT COUNT(id) AS total_clubs FROM {$wpdb->prefix}clubs");
        $sort_comp = $wpdb->get_results("SELECT COUNT(id) AS nb_per_ctgr, categorie FROM {$wpdb->prefix}clubs GROUP BY categorie");
        if (!empty ($results)) {
            ?>
            <div class="table-ranking">
                <table class="table-dark table-bordered">
                    <h2>Répartition des clubs par région</h2>
                    <tr>
                        <th>Région </th>
                        <th>Nombre de clubs </th>
                    </tr>
                    <?php
                    foreach ($results as $row) {
                        ?>
                        <tr>
                            <td><?php echo $row->region ?></td>
                            <td><?php echo $row->nb_clubs ?></td>
                        </tr>
                        <?php
                    }
                    ?>
                    <th>Total</th>
                    <td><?php foreach ($result2 as $row2) {
                            echo $row2->total_clubs;
                        }?></td>
                </table>
            </div>
            <div class="table-separation"></div>
            <?php
        }

        ?> <div class="table-separation"></div>

        <div class="table-ranking">
                <table class="table-dark table-bordered">
                    <h2>Catégories les plus pratiquées</h2>
                    <tr>
                        <th>Catégorie </th>
                        <th>Nombre de clubs pratiquants </th>
                    </tr>
                    <?php
                    foreach ($sort_comp as $row) {
                        ?>
                        <tr>
                            <td><?php echo $row->categorie ?></td>
                            <td><?php echo $row->nb_per_ctgr ?></td>
                        </tr>
                        <?php
                    }
                    ?>
                    <th>Total</th>
<!--                    <td>--><?php //foreach ($result2 as $row2) {
//                            echo $row2->total_clubs;
//                        }?><!--</td>-->
                </table>
            </div>
        <?php
    }

}